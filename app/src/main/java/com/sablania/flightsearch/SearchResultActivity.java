package com.sablania.flightsearch;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sablania.flightsearch.adapters.FlightAdapter;
import com.sablania.flightsearch.models.TravelPlanModel;
import com.sablania.flightsearch.utils.AppController;
import com.sablania.flightsearch.utils.CompareTravelPlanObject;
import com.sablania.flightsearch.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SearchResultActivity extends AppCompatActivity {
    RecyclerView recyclerView;
    TextView tvDetail;
    LinearLayout llPrice;
    ImageView ivOrderIndicator;
    FlightAdapter adapter;
    ProgressDialog progressDialog;

    List<TravelPlanModel> list;

    String searchTrackId;
    boolean isAscendingOrderOfPrice = true;
    int retryCount = 0, MAX_RETRIES = 10;
    String ACTION = "LIST_UPDATED";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        initialize();
        llPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isAscendingOrderOfPrice) {
                    isAscendingOrderOfPrice = false;
                    ivOrderIndicator.setImageResource(R.drawable.ic_arrow_drop_down_black_24dp);
                } else {
                    isAscendingOrderOfPrice = true;
                    ivOrderIndicator.setImageResource(R.drawable.ic_arrow_drop_up_black_24dp);
                }
                //this sorting can also be done in background thread
                Collections.sort(list, new CompareTravelPlanObject(isAscendingOrderOfPrice));
                adapter.notifyDataSetChanged();
            }
        });
        fetchResults();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    //receiving broadcast whenever list is updated and notifying adapter
    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if(intent.getAction().equals(ACTION)) {
                adapter.notifyDataSetChanged();
            }
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(broadcastReceiver, new IntentFilter(ACTION));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(broadcastReceiver);
    }

    private void initialize() {
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        llPrice = (LinearLayout) findViewById(R.id.ll_price);
        ivOrderIndicator = (ImageView) findViewById(R.id.iv_order_indicator);
        tvDetail = (TextView) findViewById(R.id.tv_detail);
        tvDetail.setText(getIntent().getStringExtra("detail"));

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();

        searchTrackId = getIntent().getStringExtra("search_track_id");
        list = new ArrayList<>();
        adapter = new FlightAdapter(this, list);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);
    }

    private void fetchResults(){
        try {
            retryCount +=1;
            String API = "https://pre-client-api.goomo.com/v1/flights/one_way/" + searchTrackId;

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.GET,
                    API, null,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(final JSONObject response) {
                            try {
//                                ==========================================================================================
                                //Using separate thread for sorting as this is bit costly task on main UI thread
                                //Main UI thread will get slow or blocked if we do this in main UI thread
                                //So sorting in separate thread and notifying adapter via sending broadcast receiver
                                Thread thread = new Thread(new Runnable(){
                                    @Override
                                    public void run(){
                                        Type listType = new TypeToken<ArrayList<TravelPlanModel>>() {
                                        }.getType();
                                        List<TravelPlanModel> resultData = null;
                                        try {
                                            resultData = new Gson().fromJson(response.getString("results"), listType);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }

                                        if (resultData!=null && resultData.size() > list.size()) {
                                            Collections.sort(resultData, new CompareTravelPlanObject(isAscendingOrderOfPrice));
                                            list.clear();
                                            list.addAll(resultData);
//                                            adapter.notifyDataSetChanged();
                                            Intent i = new Intent(ACTION);
                                            sendBroadcast(i);
                                        }
                                        if (!list.isEmpty()) {
                                            progressDialog.dismiss();
                                        }
                                    }
                                });
                                thread.start();



//                                tvDetail.setText("retryCount " + retryCount + " " + "total data " + list.size() + " " + response.getJSONObject("meta").getString("status"));

                                if (retryCount<MAX_RETRIES && response.getJSONObject("meta").getString("status").equals("PENDING")) {
                                    fetchResults();
                                }
                                if (list.isEmpty() && retryCount == MAX_RETRIES) {
                                    Toast.makeText(SearchResultActivity.this, "Zero result from api after " + retryCount + " retries", Toast.LENGTH_LONG).show();
                                    progressDialog.dismiss();
                                    finish();
                                }

                            } catch (JSONException e) {
                                Toast.makeText(SearchResultActivity.this, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                                progressDialog.dismiss();
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            if(list.isEmpty()) {
                                Constants.handleVolleyError(SearchResultActivity.this, error, true);
                            }
                        }
                    });

            AppController.getInstance(this).addToRequestQueue(jsonObjReq, getClass().getSimpleName());
        } catch (Exception e) {
            progressDialog.dismiss();
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        //This is to prevent memory leak
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
        super.onDestroy();
    }
}
