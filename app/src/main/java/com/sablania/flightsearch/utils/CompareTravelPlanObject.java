package com.sablania.flightsearch.utils;


import com.sablania.flightsearch.models.TravelPlanModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Comparator;


public class CompareTravelPlanObject implements Comparator<TravelPlanModel> {

    private boolean isAscendingOrder;
    public CompareTravelPlanObject(boolean isAscendingOrder) {
        this.isAscendingOrder = isAscendingOrder;
    }

    @Override
    public int compare(TravelPlanModel o1, TravelPlanModel o2) {
        JSONObject jo1, jo2;
        try {
            jo1 = new JSONObject(o1.getPricing().toString());
            jo2 = new JSONObject(o2.getPricing().toString());
            double price1 = jo1.getJSONObject("adult").getJSONObject("price").getDouble("amount");
            double price2 = jo2.getJSONObject("adult").getJSONObject("price").getDouble("amount");

            if (isAscendingOrder) {
                return (price1 > price2) ? 1 : ((price1 == price2) ? 0 : -1);
            }else {
                return (price1 < price2) ? 1 : ((price1 == price2) ? 0 : -1);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return 0;
    }
}
