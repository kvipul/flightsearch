package com.sablania.flightsearch;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.sablania.flightsearch.utils.AppController;
import com.sablania.flightsearch.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class MainActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {
    AutoCompleteTextView actvOrigin, actvDestination;
    ImageView ivSwap;
    Button btnSearch;
    EditText etAdultCount;
    Spinner spnClass;
    ArrayAdapter<String> airportAdapter, classesAdapter;
    TextView tvDate;

    List<String> airports = Arrays.asList( "BOM", "DEL", "MAA", "BLR", "GOI", "CCU", "PNQ", "JAI", "IXC", "HYD");
    String[] classes = { "ECONOMY", "BUSINESS", "FIRST_CLASS", "PREMIUM_ECONOMY"};
    String API = "https://pre-client-api.goomo.com/v1/flights/one_way/search";

    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();
        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Constants.hideInputKeyboard(MainActivity.this, etAdultCount);
                initiateSearch();
            }
        });
        ivSwap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String temp = actvOrigin.getText().toString();
                actvOrigin.setText(actvDestination.getText());
                actvDestination.setText(temp);
                actvDestination.dismissDropDown();
            }
        });
        tvDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datePickerDialog.show();
            }
        });
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
//        Log.e("time", year + "-" + month + "-" + dayOfMonth);
        tvDate.setText(Constants.getStandardFormat(dayOfMonth, month+1, year));
    }

    private void initialize() {
        actvOrigin = (AutoCompleteTextView) findViewById(R.id.actv_origin);
        actvDestination = (AutoCompleteTextView) findViewById(R.id.actv_destination);
        spnClass = (Spinner) findViewById(R.id.spn_class);
        btnSearch = (Button) findViewById(R.id.btn_search);
        etAdultCount = (EditText) findViewById(R.id.et_adult_count);
        tvDate = (TextView) findViewById(R.id.tv_date);
        ivSwap = (ImageView) findViewById(R.id.iv_swap);

        airportAdapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1, airports);

        actvOrigin.setAdapter(airportAdapter);
        actvOrigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actvOrigin.showDropDown();
            }
        });
        actvDestination.setAdapter(airportAdapter);
        actvDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actvDestination.showDropDown();
            }
        });
        actvDestination.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Constants.hideInputKeyboard(MainActivity.this, actvDestination);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        classesAdapter = new ArrayAdapter<String>(MainActivity.this,
                android.R.layout.simple_list_item_1, classes);
        spnClass.setAdapter(classesAdapter);

        Calendar cal = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, this, cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

    }

    private void initiateSearch() {
        try {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.setCanceledOnTouchOutside(false);

            final String origin = actvOrigin.getText().toString().toUpperCase();
            if (origin.isEmpty()) {
                Toast.makeText(MainActivity.this, "Please choose origin airport", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!airports.contains(origin)) {
                Toast.makeText(MainActivity.this, "Please choose valid origin airport", Toast.LENGTH_SHORT).show();
                return;
            }
            final String destination = actvDestination.getText().toString().toUpperCase();
            if (destination.isEmpty()) {
                Toast.makeText(MainActivity.this, "Please choose destination airport", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!airports.contains(destination)) {
                Toast.makeText(MainActivity.this, "Please choose valid destination airport", Toast.LENGTH_SHORT).show();
                return;
            }
            if (tvDate.getText().toString().equalsIgnoreCase("CHOOSE DATE")) {
                Toast.makeText(MainActivity.this, "Please choose a date first", Toast.LENGTH_SHORT).show();
                return;
            }
            if (etAdultCount.getText().toString().isEmpty()) {
                Toast.makeText(MainActivity.this, "Choose total no. of adult", Toast.LENGTH_SHORT).show();
                return;
            }
            final int adultCount = Integer.parseInt(etAdultCount.getText().toString());

            String cls = spnClass.getSelectedItem().toString();

            progressDialog.show();

            JSONObject requestObject = new JSONObject();

            requestObject.put("adult_count", adultCount);
            requestObject.put("child_count", 0);
            requestObject.put("class_type", cls);
            requestObject.put("destination", new JSONObject().put("airport", destination));
            requestObject.put("infant_count", 0);
            requestObject.put("origin", new JSONObject().put("airport", origin));
            requestObject.put("residentof_india", true);
            requestObject.put("travel_date", tvDate.getText().toString());

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    API, requestObject,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {
                            progressDialog.dismiss();
                            try {
                                Intent intent = new Intent(MainActivity.this, SearchResultActivity.class);
                                intent.putExtra("search_track_id", response.getJSONObject("meta").getString("search_track_id"));
                                intent.putExtra("detail", origin + " To " + destination + " | Adult: " + adultCount + " | " + tvDate.getText());
                                startActivity(intent);
                            } catch (JSONException e) {
                                Toast.makeText(MainActivity.this, "Something went wrong. Please try again.", Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            progressDialog.dismiss();
                            Constants.handleVolleyError(MainActivity.this, error, true);
                        }
                    });

            AppController.getInstance(this).addToRequestQueue(jsonObjReq, getClass().getSimpleName());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
