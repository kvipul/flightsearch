package com.sablania.flightsearch.models;

import com.google.gson.annotations.SerializedName;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

public class FlightModel implements Serializable {

    @SerializedName("origin")
    String originCode;

    @SerializedName("destination")
    String destinationCode;

    @SerializedName("departure_datetime")
    String departureTime;

    @SerializedName("arrival_datetime")
    String arrivalTime;

    @SerializedName("travel_duration")
    String travelDuration;

    @SerializedName("airline_code")
    String airlineCode;

    @SerializedName("pricing")
    List<JSONObject> pricing;

    @SerializedName("number_of_stops")
    int noOfStops;


    public String getOriginCode() {
        return originCode;
    }

    public void setOriginCode(String originCode) {
        this.originCode = originCode;
    }

    public String getDestinationCode() {
        return destinationCode;
    }

    public void setDestinationCode(String destinationCode) {
        this.destinationCode = destinationCode;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(String arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getTravelDuration() {
        return travelDuration;
    }

    public void setTravelDuration(String travelDuration) {
        this.travelDuration = travelDuration;
    }

    public String getAirlineCode() {
        return airlineCode;
    }

    public void setAirlineCode(String airlineCode) {
        this.airlineCode = airlineCode;
    }

    public List<JSONObject> getPricing() {
        return pricing;
    }

    public void setPricing(List<JSONObject> pricing) {
        this.pricing = pricing;
    }

    public int getNoOfStops() {
        return noOfStops;
    }

    public void setNoOfStops(int noOfStops) {
        this.noOfStops = noOfStops;
    }
}
