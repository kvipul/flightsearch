package com.sablania.flightsearch.models;


import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class TravelPlanModel implements Serializable {

    @SerializedName("travel_plan_id")
    String travelPlanId;

    @SerializedName("flights")
    List<FlightModel> flights;

    @SerializedName("pricing")
    Object pricing;

    @SerializedName("refundable")
    boolean refundable;

    public String getTravelPlanId() {
        return travelPlanId;
    }

    public void setTravelPlanId(String travelPlanId) {
        this.travelPlanId = travelPlanId;
    }

    public List<FlightModel> getFlights() {
        return flights;
    }

    public void setFlights(List<FlightModel> flights) {
        this.flights = flights;
    }

    public Object getPricing() {
        return pricing;
    }

    public void setPricing(Object pricing) {
        this.pricing = pricing;
    }

    public boolean isRefundable() {
        return refundable;
    }

    public void setRefundable(boolean refundable) {
        this.refundable = refundable;
    }
}
