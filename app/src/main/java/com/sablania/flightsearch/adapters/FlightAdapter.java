package com.sablania.flightsearch.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sablania.flightsearch.R;
import com.sablania.flightsearch.models.FlightModel;
import com.sablania.flightsearch.models.TravelPlanModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class FlightAdapter extends RecyclerView.Adapter<FlightAdapter.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;

    private List<TravelPlanModel> list;

    public FlightAdapter(Context context, List<TravelPlanModel> list) {
        this.context = context;
        this.list = list;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public FlightAdapter.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.cv_flight_list_item, parent, false);

        return new ViewHolder(convertView);
    }

    @Override
    public void onBindViewHolder(final FlightAdapter.ViewHolder holder, final int position) {
        final TravelPlanModel travelPlanModel = list.get(position);
        FlightModel flightModel = travelPlanModel.getFlights().get(0);

        holder.tvTravelTime.setText(" | " + flightModel.getTravelDuration() + " | ");
        if (flightModel.getNoOfStops() == 0) {
            holder.tvStops.setText("NonStop");
        } else {
            holder.tvStops.setText(String.valueOf(flightModel.getNoOfStops()));
        }
        holder.tvDetail.setText(flightModel.getOriginCode() + " To " + flightModel.getDestinationCode());
        holder.tvAirline.setText("Airline-" + flightModel.getAirlineCode());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
        try {
            holder.tvDepartureTime.setText(sdf2.format(sdf.parse(flightModel.getDepartureTime())));
            holder.tvArrivalTime.setText(sdf2.format(sdf.parse(flightModel.getArrivalTime())));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            JSONObject joPrice = new JSONObject(travelPlanModel.getPricing().toString());
            JSONObject price = joPrice.getJSONObject("adult").getJSONObject("price");
            holder.tvPrice.setText(price.getString("currency") + " " + price.getString("amount"));

        } catch (JSONException e) {
            e.printStackTrace();
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //set action
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvAirline, tvDepartureTime, tvArrivalTime,tvPrice, tvStops, tvDetail, tvTravelTime;

        ViewHolder(View itemView) {
            super(itemView);

            tvAirline = (TextView) itemView.findViewById(R.id.tv_airline);
            tvDepartureTime = (TextView) itemView.findViewById(R.id.tv_departure_time);
            tvArrivalTime = (TextView) itemView.findViewById(R.id.tv_arrival_time);
            tvStops = (TextView) itemView.findViewById(R.id.tv_stops);
            tvDetail = (TextView) itemView.findViewById(R.id.tv_detail);
            tvTravelTime = (TextView) itemView.findViewById(R.id.tv_travel_time);
            tvPrice = (TextView) itemView.findViewById(R.id.tv_price);
        }
    }
}
