# Flight Search App
Development Time: 6-7 Hrs


## Aim
Build a flight Search app​ that should allow user to search for a flight using given api. Api will responds in parts so poll the api until you get success status and display result in a list view with some detail. Sort functionality on flight price field is bonus.

## App Features
- On the home screen user has to choose required fields and search for results
- To make scrolling better i've used separate thread to do sorting of data and notifying adapter using broadcast receiver
- Views are made in such a way that app will work fine in both screen orientations
- Vector drawables are used for icons which basically use less memory and can be scaled
- Code has been written in separate modules to remove code duplication, maintain better code structure and to make app easily scalable
- Proguard is also used for release build to make app size small and to make app more secure

## Third Party libraries and References
- Volley: For Network Call
- Data Source: [Api provided by Goomo](https://www.goomo.com/)
 